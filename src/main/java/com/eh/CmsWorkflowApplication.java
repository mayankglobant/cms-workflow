package com.eh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CmsWorkflowApplication {

	public static void main(String[] args) {
		SpringApplication.run(CmsWorkflowApplication.class, args);
	}
}
